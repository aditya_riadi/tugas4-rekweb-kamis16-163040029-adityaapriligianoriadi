<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Login extends CI_Controller
{
	public function __construct(){
		parent::__construct();
		$this->load->model('Login_Model');
		$this->load->library('form_validation');
	}

	public function index(){
		$this->load->view('login/index');
	}

	public function aksi_login(){

		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'password' => md5($password)
		);
		$cek = $this->Login_Model->cek_login('user',$where)->num_rows();
		if ($cek > 0) {
			$data_session = array(
				'username' => $username,
				'status' => "login"
			);
			$this->session->set_userdata($data_session);
			redirect(site_url('tokoMainan/index'));
		}else{
			redirect(site_url('login/index'));
		}
		
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect(site_url('login/index'));
	}

	public function register(){
		$login = $this->Login_Model;
		$validation = $this->form_validation;
		$validation->set_rules($login->rules());

		if ($validation->run()) {
			if ($this->input->post('daftar')) {
				$login->register();
				$this->session->set_flashdata('Success', 'Berhasil Registrasi');
				// redirect('login/index');
			}else{
				$this->load->view('login/registrasi');
			}	
		}

		$this->load->view('login/registrasi');
		
	}

}