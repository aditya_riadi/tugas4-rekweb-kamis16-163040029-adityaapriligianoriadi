<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class TokoMainan extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('TokoMainan_model');
		$this->load->library('cart');
		if ($this->session->userdata('status') != "login") {
			redirect(site_url('Login/index'));
		}
		$this->API = "http://localhost/tugas3-rekweb-kamis16-163040029-adityaapriligianoriadi";

	}

	public function index(){
		$data['mainan']  = json_decode($this->curl->simple_get($this->API . '/mainan/'));
		$this->load->view('tokomainan/index', $data);
	}

	public function detail($id){
		$data['mainan'] = json_decode($this->curl->simple_get($this->API.'/mainan/',array("id_mainan" => $id)));
		$this->load->view('tokomainan/detail', $data);
	}
	

	public function tampil_cart()
	{
		$data['mainan'] = json_decode($this->curl->simple_get($this->API . '/mainan/'));
		$this->load->view('tokomainan/tampil_cart',$data);
	}
	
	
	function tambah()
	{	$getData = json_decode($this->curl->simple_get($this->API.'/mainan/',array("id_mainan" => $id)));

		$data_produk = array('id' => $this->input->post('id'),
							 'name' => $this->input->post('nama'),
							 'price' => $this->input->post('harga'),
							 'gambar' => $this->input->post('gambar'),
							 'qty' => $this->input->post('qty')
							);
		
		$this->cart->insert($data_produk);
		redirect('tokomainan');
	}

	function hapus($rowid) 
	{
		if ($rowid=="all")
			{
				$this->cart->destroy();
			}
		else
			{
				$data = array('rowid' => $rowid,
			  				  'qty' =>0);
				$this->cart->update($data);
			}
		redirect('tokomainan/tampil_cart');
	}

	function ubah_cart()
	{
		$cart_info = $_POST['cart'] ;
		foreach( $cart_info as $id => $cart)
		{
			$rowid = $cart['rowid'];
			$price = $cart['price'];
			$gambar = $cart['gambar'];
			$amount = $price * $cart['qty'];
			$qty = $cart['qty'];
			$data = array('rowid' => $rowid,
							'price' => $price,
							'gambar' => $gambar,
							'amount' => $amount,
							'qty' => $qty);
			$this->cart->update($data);
		}
		redirect('tokomainan/tampil_cart');
	}

	public function check_out()
	{
		$data['mainan'] = json_decode($this->curl->simple_get($this->API . '/mainan/'));
		$this->load->view('tokomainan/check_out', $data);
	}

	public function check_out2(){
		$data_produk= array('id' => $this->input->post('id'),
							 'name' => $this->input->post('nama'),
							 'price' => $this->input->post('harga'),
							 'gambar' => $this->input->post('gambar'),
							 'qty' =>$this->input->post('qty')
							);
		$this->cart->insert($data_produk);
		redirect('tokomainan/check_out');
	}
	
	public function proses_order()
	{
		//-------------------------Input data pelanggan--------------------------
		$data_pelanggan = array('nama' => $this->input->post('nama'),
							'email' => $this->input->post('email'),
							'alamat' => $this->input->post('alamat'),
							'telp' => $this->input->post('telp'));
		$id_pelanggan = $this->TokoMainan_model->tambah_pelanggan($data_pelanggan);
		//-------------------------Input data order------------------------------
		$data_order = array('tanggal' => date('Y-m-d'),
					   		'pelanggan' => $id_pelanggan);
		$id_order = $this->TokoMainan_model->tambah_order($data_order);
		//-------------------------Input data detail order-----------------------		
		if ($cart = $this->cart->contents())
			{
				foreach ($cart as $item)
					{
						$data_detail = array('order_id' =>$id_order,
										'mainan' => $item['id'],
										'qty' => $item['qty'],
										'harga' => $item['price']);			
						$proses = $this->TokoMainan_model->tambah_detail_order($data_detail);
					}
			}
		//-------------------------Hapus shopping cart--------------------------		
		$this->cart->destroy();
		$data['mainan'] = json_decode($this->curl->simple_get($this->API . '/mainan/'));
		$this->load->view('tokomainan/sukses',$data);
	}
	


}