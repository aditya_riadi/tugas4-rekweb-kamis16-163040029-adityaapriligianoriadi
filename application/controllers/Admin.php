<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Admin extends CI_Controller
{

	public function __construct(){
		parent::__construct();
		$this->load->model('Admin_mainan_model');
		$this->load->library('form_validation');
		if ($this->session->userdata('status') != "login") {
			redirect(site_url('Login_Admin/index'));
		}
		$this->API = "http://localhost/tugas3-rekweb-kamis16-163040029-adityaapriligianoriadi";
	}

	public function index()
	{
		
		$data['mainan']  = json_decode($this->curl->simple_get($this->API . '/mainan/'));
		$data['content'] = 'admin/index';
		$this->load->view('templates/template',$data);
	}

	public function rules(){
		return[
			[
				'field'=>'nama_mainan',
				'label'=>'Nama_mainan',
				'rules'=>'required'
			],
			[
				'field'=>'stok_mainan',
				'label'=>'Stok_mainan',
				'rules'=>'numeric'
			],
			[
				'field'=>'harga_mainan',
				'label'=>'Harga_mainan',
				'rules'=>'numeric'
			],
			[
				'field'=>'deskripsi_mainan',
				'label'=>'Deskripsi_mainan',
				'rules'=>'required'
			]
		];
	}

	public function tambah_aksi() {

		// $mainan = $this->Admin_mainan_model;
		$id_mainan = uniqid();
		$nama_mainan = $this->input->post('nama_mainan');
		$harga_mainan = $this->input->post('harga_mainan');
		$stok_mainan = $this->input->post('stok_mainan');
		$deskripsi_mainan = $this->input->post('deskripsi_mainan');
		$img_mainan = "default.jpg";

		$data = array(
			'id_mainan'=> $id_mainan,
			'nama_mainan' => $nama_mainan,
			'harga_mainan'=>$harga_mainan,
			'stok_mainan'=>$stok_mainan,
			'deskripsi_mainan'=>$deskripsi_mainan,
			'img_mainan'=>$img_mainan
		);

		$validation = $this->form_validation;
		$validation->set_rules($this->rules());
		
		if ($validation->run()) {
			// $mainan->tambahMainan();
			$this->curl->simple_post($this->API.'/mainan/', $data, array(CURLOPT_BUFFERSIZE => 10));
			json_encode(array("status"=>true));
			$this->session->set_flashdata('Success', 'Berhasil disimpan');
		}

		$data['content'] = 'admin/tambah';
		$this->load->view('templates/template', $data);
	}

	public function hapus($id_mainan = null){
		if (!isset($id_mainan)) show_404();

		json_decode($this->curl->simple_delete($this->API.'/mainan/', array('id_mainan'=>$id_mainan), array(CURLOPT_BUFFERSIZE => 10)));
		echo json_encode(array("status"=>TRUE));
		redirect(site_url('admin'));
	}

	public function edit($id_mainan)
	{
		$params = array("id_mainan"=>$id_mainan);
		$data['mainan'] = json_decode($this->curl->simple_get($this->API.'/mainan/', $params));
		$mainan = $data['mainan'];
		$json = json_encode(array("status"=>200, "mainan"=>$mainan));
		// echo $json;
		$data['content'] = 'admin/ubah';

		if (!$data['mainan']) show_404();
		$this->load->view('templates/template',$data);
	}

	public function ubah($id_mainan = null){
		if (!isset($id_mainan)) redirect('admin/index');

		$id_mainan = $this->input->post('id_mainan');
		$nama_mainan = $this->input->post('nama_mainan');
		$harga_mainan = $this->input->post('harga_mainan');
		$stok_mainan = $this->input->post('stok_mainan');
		$deskripsi_mainan = $this->input->post('deskripsi_mainan');
		$img_mainan = $this->input->post('old_img');

		$data = array(
			'nama_mainan' => $nama_mainan,
			'harga_mainan'=> $harga_mainan,
			'stok_mainan'=> $stok_mainan,
			'deskripsi_mainan'=> $deskripsi_mainan,
			'img_mainan'=> $img_mainan,
		);
		
		$validation = $this->form_validation;
		$validation->set_rules($this->rules());

		if ($validation->run()) {
			$this->curl->simple_put($this->API.'/mainan/', $data, array(CURLOPT_BUFFERSIZE => 10));
			echo json_encode(array("status"=>TRUE));
			$this->session->set_flashdata('Success', 'Berhasil disimpan');
		}
		
	}

	
}