<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Login_Admin_Model extends CI_Model
{
	private $_table = 'admin';
	public $id;
	public $fullname;
	public $password;
	
	public function rules(){
		return[
			[
				'field'=>'fullname',
				'label'=>'FullName',
				'rules'=>'required'
			],
			[
				'field'=>'username',
				'label'=>'UserName',
				'rules'=>'alpha_numeric'
			],
			[
				'field'=>'password',
				'label'=>'Password',
				'rules'=>'alpha_numeric'
			]
		];
	}

	public function cek_login($_table, $where){
		return $this->db->get_where($_table, $where);
	}

	public function register(){
		$post = $this->input->post();
		$this->id = uniqid();
		$this->fullname = $post['fullname'];
		$this->username = $post['username'];
		$this->password = md5($post['password']);

		$this->db->insert($this->_table, $this);
	}
	
}