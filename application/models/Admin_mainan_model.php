<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Admin_mainan_model extends CI_Model
{
	private $_table = "mainan";
	public $id_mainan;
	public $nama_mainan;
	public $harga_mainan;
	public $stok_mainan;
	public $deskripsi_mainan;
	public $img_mainan = "default.jpg";

	public function rules(){
		return[
			[
				'field'=>'nama_mainan',
				'label'=>'Nama_mainan',
				'rules'=>'required'
			],
			[
				'field'=>'stok_mainan',
				'label'=>'Stok_mainan',
				'rules'=>'numeric'
			],
			[
				'field'=>'harga_mainan',
				'label'=>'Harga_mainan',
				'rules'=>'numeric'
			],
			[
				'field'=>'deskripsi_mainan',
				'label'=>'Deskripsi_mainan',
				'rules'=>'required'
			]
		];
	}

	public function getMainan(){
		return $this->db->get($this->_table)->result();
	}

	public function getMainanById($id){
		return $this->db->get_where($this->_table, ['id_mainan'=>$id])->row();
	}


	public function ubahMainan($id){
		$post = $this->input->post();
		$this->id_mainan = $post['id'];
		$this->nama_mainan = $post['nama_mainan'];
		$this->harga_mainan = $post['harga_mainan'];
		$this->stok_mainan = $post['stok_mainan'];
		$this->deskripsi_mainan = $post['deskripsi_mainan'];
		
		if (!empty($_FILES['img_mainan']['nama_mainan'])) {
			$this->img_mainan = $this->_uploadImage();
		}else{
			$this->img_mainan = $post['old_img'];
		}

		$this->db->update($this->_table, $this, array('id_mainan'=>$post['id']));
	}


	private function _uploadImage(){
		$config['upload_path'] = './assets/img/upload/product/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['file_name'] = $this->id_mainan;
		$config['overwrite'] = true;
		$config['max_size']  = '100';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
		
		$this->load->library('upload', $config);
		
		if ($this->upload->do_upload('img_mainan')){
			return $this->upload->data('file_name');
		}
		return "default.jpg";
	}
	
	private function _deleteImage($id){
		$mainan = $this->getMainanById($id);
		if ($mainan->img_mainan != "default.jpg") {
			$filename = explode(".", $mainan->img_mainan)[0];
			return array_map('unlink', glob(FCPATH."assets/img/upload/product/$filename.*"));
		}
	}
}