
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class TokoMainan_model extends CI_Model
{
	

	public function tambah_pelanggan($data)
	{
		$this->db->insert('pelanggan', $data);
		$id = $this->db->insert_id();
		return (isset($id)) ? $id : FALSE;
	}
	
	public function tambah_order($data)
	{
		$this->db->insert('order', $data);
		$id = $this->db->insert_id();
		return (isset($id)) ? $id : FALSE;
	}
	
	public function tambah_detail_order($data)
	{
		$this->db->insert('detail_order', $data);
	}

}