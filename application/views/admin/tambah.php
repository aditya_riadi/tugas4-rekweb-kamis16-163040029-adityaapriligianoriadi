<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">

    <div class="row">
        <div class="col-md-12">

            <?php if ($this->session->flashdata('Success')) :?>
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <?= $this->session->flashdata('Success'); ?>
                </div>
            <?php endif; ?>
            <form class="form-horizontal" action="<?php echo site_url('admin/tambah_aksi')?>" method="post" enctype="multipart/form-data">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Tambah Mainan</strong> Baru</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">                                                                        
                    
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Nama Mainan <span class="label label-default">wajib</span></label>
                        <div class="col-md-6 col-xs-12">                                            
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                <input type="text" class="form-control <?= form_error('nama_mainan') ? 'is-invalid':'' ?>" placeholder="Masukan Nama Mainan" name="nama_mainan" />
                            </div>     
                            <span class="help-block">Tulis nama mainan sesuai rincian mainan</span>                                       
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Harga <span class="label label-default">wajib</span></label>
                        <div class="col-md-6 col-xs-12">
                            <span style="color: red;"><?php echo form_error('stok_mainan') ?></span>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-money"></span></span>
                                <input type="text" class="form-control <?= form_error('harga_mainan') ? 'is-invalid':'' ?>" placeholder="Masukan Harga" name="harga_mainan"/>
                            </div>   
                            <span class="help-block">Dalam mata uang Rupiah</span>                                         
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Stok <span class="label label-default">wajib</span></label>
                        <div class="col-md-6 col-xs-12">
                            <span style="color: red;"><?php echo form_error('stok_mainan') ?></span>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                <input type="text" class="form-control <?= form_error('stok_mainan') ? 'is-invalid':'' ?>" placeholder="Masukan Stok Mainan" name="stok_mainan"/>
                            </div>         
                            <span class="help-block">lebih dari atau sama dengan 1</span>                                   
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Deskripsi <span class="label label-default">wajib</span></label>
                        <div class="col-md-6 col-xs-12"> 
                        <span class="help-block">Deskripsikan produk secara lengkap & jelas. Rekomendasi panjang: >= 1000 karakter.</span>
                            <textarea class="form-control <?= form_error('deskripsi_mainan') ? 'is-invalid':'' ?>" rows="5" placeholder="Masukan Deskripsi Mainan" name="deskripsi_mainan" ></textarea>
                        </div>

                    </div>      
                    
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Gambar</label>
                        <div class="col-md-6 col-xs-12">                                               
                            <input type="file" class="fileinput btn-primary <?= form_error('img_mainan') ? 'is-invalid':'' ?>" name="img_mainan" id="img_mainan" title="Browse file"/>
                            <span class="help-block">masukan gambar exstensi .jpg atau .png</span>
                        </div>
                    </div>

                </div>
                <div class="panel-footer">                                  
                    <button class="btn btn-primary pull-right" type="submit">Tambah</button>
                </div>
            </div>
            </form>
            
        </div>
    </div>                    
    
</div>
<!-- END PAGE CONTENT WRAPPER