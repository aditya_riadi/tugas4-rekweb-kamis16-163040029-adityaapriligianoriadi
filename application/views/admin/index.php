             
    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">                
        <div class="row">
            <div class="col-md-12">
                <!-- START DEFAULT DATATABLE -->
                <div class="panel panel-default">
                    <div class="panel-heading">                                
                        <h3 class="panel-title">Daftar Mainan</h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                            <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                        </ul>                                
                    </div>
                    <div class="panel-body">
                        <form action="">
                        <table class="table datatable">
                            <thead>
                                <tr>
                                    <th>ID Mainan</th>
                                    <th>Nama Mainan</th>
                                    <th>Harga Mainan</th>
                                    <th>Stok</th>
                                    <th>Deskripsi</th>
                                    <th>Gambar</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($mainan as $mnm) :?>
                                    <tr>
                                        <td><?= $mnm->id_mainan ?></td>
                                        <td><?= $mnm->nama_mainan ?></td> 
                                        <td>Rp.<?= $mnm->harga_mainan ?></td>
                                        <td><?= $mnm->stok_mainan ?></td>
                                        <td><?= $mnm->deskripsi_mainan ?></td>
                                        <td><img src="<?= base_url('assets/img/upload/product/'.$mnm->img_mainan) ?>" alt="" class="img-thumbnail"></td>
                                        <td>
                                                <a href="<?= site_url('admin/edit/'.$mnm->id_mainan) ?>" class="btn btn-info btn-block">ubah</a>
                                                <a onclick="deleteConfirm('<?= site_url('admin/hapus/'.$mnm->id_mainan) ?>')" href="#!" class="btn btn-danger btn-block">hapus</a>
                                        </td>   
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                        </form>
                    </div>
                </div>
                <!-- END DEFAULT DATATABLE -->             

   








