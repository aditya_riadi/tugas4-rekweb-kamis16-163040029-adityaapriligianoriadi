<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">

    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal" action="<?= site_url('admin/ubah/'.$mainan[0]->id_mainan)?>" method="post" enctype="multipart/form-data">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Ubah Mainan</strong></h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">                          
                    <input type="hidden" name="id_mainan"  value="<?= $mainan[0]->id_mainan ?>" >                                         
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Nama Mainan <span class="label label-default">wajib</span></label>
                        <div class="col-md-6 col-xs-12">                                            
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                <input type="text" class="form-control <?= form_error('nama_mainan') ? 'is-invalid':'' ?>" placeholder="Masukan Nama Mainan" name="nama_mainan" value="<?= $mainan[0]->nama_mainan ?>" />
                            </div>     
                            <div class="invalid-feedback">
                                <?php echo form_error('nama_mainan'); ?>
                            </div>
                            <span class="help-block">Tulis nama mainan sesuai rincian mainan</span>                                       
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Harga <span class="label label-default">wajib</span></label>
                        <div class="col-md-6 col-xs-12">                                            
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                <input type="text" class="form-control <?= form_error('harga_mainan') ? 'is-invalid':'' ?>" placeholder="Masukan Harga" name="harga_mainan" value="<?= $mainan[0]->harga_mainan ?>"/>
                            </div>
                            <div class="invalid-feddback">
                                <?php echo form_error('harga_mainan'); ?>
                            </div>   
                            <span class="help-block">Dalam mata uang Rupiah</span>                                         
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Stok <span class="label label-default">wajib</span></label>
                        <div class="col-md-6 col-xs-12">                                            
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                <input type="text" class="form-control <?= form_error('stok_mainan') ? 'is-invalid':'' ?>" placeholder="Masukan Stok Mainan" name="stok_mainan" value="<?= $mainan[0]->stok_mainan ?>"/>
                            </div>
                            <div class="invalid-feedback">
                                <?php echo form_error('stok_mainan'); ?>
                            </div>         
                            <span class="help-block">lebih dari atau sama dengan 1</span>                                   
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Deskripsi <span class="label label-default">wajib</span></label>
                        <div class="col-md-6 col-xs-12"> 
                        <span class="help-block">Deskripsikan produk secara lengkap & jelas. Rekomendasi panjang: >= 1000 karakter.</span>
                            <textarea class="form-control <?= form_error('deskripsi_mainan') ? 'is-invalid':'' ?>" rows="5" placeholder="Masukan Deskripsi Mainan" name="deskripsi_mainan"><?= $mainan[0]->deskripsi_mainan ?></textarea>
                        </div>
                        <div class="invalid-feedback">
                            <?php echo form_error('deskripsi_mainan'); ?>
                        </div>

                    </div>      
                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Gambar</label>
                        <div class="col-md-6 col-xs-12">
                            <input type="file" class="fileinput btn-primary <?= form_error('img_mainan') ? 'is-invalid':'' ?>" name="img_mainan" id="img_mainan" title="Browse file"/>
                            <input type="hidden" name="old_img" value="<?=$mainan[0]->img_mainan ?>">
                            <span class="help-block">masukan gambar exstensi .jpg atau .png</span>
                            <div class="invalid-feedback">
                                <?php echo form_error('img_mainan'); ?>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="panel-footer">                                  
                    <button class="btn btn-primary pull-right" type="submit">Ubah</button>
                </div>
            </div>
            </form>
           
            
        </div>
    </div>                    
    
</div>
<!-- END PAGE CONTENT WRAPPER -->