<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Jual Beli Online Mainan</title>

    <!-- Bootstrap core CSS -->
    <link href="<?= base_url() ?>assets/css/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?= base_url() ?>assets/css/shop-homepage.css" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="<?= site_url('tokomainan') ?>"><img src="<?= base_url() ?>assets/img/tokoMainan/iconTm.png" alt="" width="80%"></a>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a href="<?= site_url('tokoMainan/tampil_cart') ?>">
                <i><img src="<?= base_url() ?>assets/img/tokoMainan/cart.png" alt="" width="40%"></i>
              </a>
            </li>
            <li class="nav-item">
              <a href="" class="mb-control" data-box="#mb-signout" >
                <i><img src="<?= base_url()  ?>assets/img/tokoMainan/out-sign.png" alt="" width="40%"></i>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">

      <div class="row">
        <div class="col-lg-12">

          <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
            <ol class="carousel-indicators">
              <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
              <div class="carousel-item active">
                <img class="d-block img-fluid" src="<?= base_url() ?>assets/img/tokoMainan/tm3.png" alt="First slide">
              </div>
              <div class="carousel-item">
                <img class="d-block img-fluid" src="<?= base_url() ?>assets/img/tokoMainan/tm2.png" alt="Second slide">
              </div>
              <div class="carousel-item">
                <img class="d-block img-fluid" src="<?= base_url() ?>assets/img/tokoMainan/tm1.png" alt="Third slide">
              </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>

        </div>

        <div class="col-lg-12">
          <div class="row">
            <?php foreach ($mainan as $key) :?>
              
              <div class="col-lg-4 col-md-6 mb-4">
                <form action="<?php echo site_url('tokomainan/tambah')?>" method="post" accept-charset="utf-8">
                <div class="card h-100">
                  <a href="<?= site_url('TokoMainan/detail/'.$key->id_mainan) ?>"><img class="card-img-top" src="<?= base_url('assets/img/upload/product/'.$key->img_mainan) ?>" alt=""></a>
                  <div class="card-body">
                    <h4 class="card-title">
                      <a href="<?= site_url('TokoMainan/detail/'.$key->id_mainan) ?>" style="color: black;"><?= $key->nama_mainan ?></a>
                    </h4>
                    <h5>Rp.<?php echo number_format($key->harga_mainan,0,",",".");?></h5>
                    <h6 class="text-muted">Stok : <?= $key->stok_mainan ?></h6 class="text-muted">
                  </div>
                  <div class="card-footer">
                    <input type="hidden" name="id" value="<?php echo $key->id_mainan ?>" />
                    <input type="hidden" name="nama" value="<?php echo $key->nama_mainan ?>" />
                    <input type="hidden" name="harga" value="<?php echo $key->harga_mainan ?>" />
                    <input type="hidden" name="gambar" value="<?php echo $key->img_mainan ?>" />
                    <input type="hidden" name="qty" value="1" />
                    <div class="row">
                      <div class="col-6 mr-md-auto">
                        <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small> 
                      </div>
                      <div class="col-6">
                        <button type="submit" class="btn btn-sm btn-warning"><img src="<?= base_url() ?>assets/img/tokoMainan/cart.png" alt="" width="30%"> <strong style="color: white">Beli</strong></button>
                      </div>
                    </div>


                  </div>
                </div>
                </form>
              </div>
          <?php endforeach ?>
          </div>
          <!-- /.row -->
        </div>
        <!-- /.col-lg-12 -->

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-4 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">TokoMainan Copyright &copy; Your Website 2018</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="<?= site_url('Login/logout') ?>" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MESSAGE BOX-->

    <!-- START PRELOADS -->
    <audio id="audio-alert" src="<?= base_url() ?>assets/audio/alert.mp3" preload="auto"></audio>
    <audio id="audio-fail" src="<?= base_url() ?>assets/audio/fail.mp3" preload="auto"></audio>
    <!-- END PRELOADS -->  

    <!-- Bootstrap core JavaScript -->
    <script src="<?= base_url() ?>assets/css/bootstrap/jquery/jquery.js"></script>
    <script src="<?= base_url() ?>assets/css/bootstrap/js/bootstrap.bundle.min.js"></script>
    
    <!-- PLUGIN -->
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/bootstrap/bootstrap.min.js"></script>        
    <!-- END PLUGINS -->                

    <!-- THIS PAGE PLUGINS -->
    <script type='text/javascript' src='<?= base_url() ?>assets/js/plugins/icheck/icheck.min.js'></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
    
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/datatables/jquery.dataTables.min.js"></script>    

    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/bootstrap/bootstrap-datepicker.js"></script>                
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/bootstrap/bootstrap-file-input.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/bootstrap/bootstrap-select.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/tagsinput/jquery.tagsinput.min.js"></script>
    <!-- END PAGE PLUGINS -->

    <!-- START TEMPLATE -->
    
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins.js"></script>        
    <script type="text/javascript" src="<?= base_url() ?>assets/js/actions.js"></script>        
    <!-- END TEMPLATE -->
    <!-- END SCRIPTS --> 

  </body>

</html>
