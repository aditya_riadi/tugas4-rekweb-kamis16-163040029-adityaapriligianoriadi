<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Jual Beli Online Mainan</title>

    <!-- Bootstrap core CSS -->
    <link href="<?= base_url() ?>assets/css/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?= base_url() ?>assets/css/shop-homepage.css" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="<?= site_url('tokomainan') ?>"><img src="<?= base_url() ?>assets/img/tokoMainan/iconTm.png" alt="" width="80%"></a>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a href="<?= site_url('tokomainan/tampil_cart') ?>">
                <i><img src="<?= base_url() ?>assets/img/tokoMainan/cart.png" alt="" width="40%"></i>
              </a>
            </li>
            <li class="nav-item">
              <a href="" class="mb-control" data-box="#mb-signout" >
                <i><img src="<?= base_url()  ?>assets/img/tokoMainan/out-sign.png" alt="" width="40%"></i>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-12 py-2">
          <form method="post" action="<?php echo site_url('tokomainan/check_out2')?>" method="post" accept-charset="utf-8">
          <div class="row">
              <div class="col-lg-4 col-md-6 mb-4">
                <div class="card h-100">
                  <a href="#"><img class="card-img-top" src="<?= base_url('assets/img/upload/product/'.$mainan[0]->img_mainan) ?>" alt=""></a>
                </div>
              </div>
              <div class="col-lg-8 col-md-6 mb-4">
                <div class="row">
                  <div class="col-lg-12">
                    <h1 class="text-center"><?= $mainan[0]->nama_mainan ?></h1>                    
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734; 0 Ulasan</small>
                    <small class="text-muted">0  Transaksi Sukses Dari  0  Transaksi </small>
                  </div>
                </div>
                <div class="row py-1">
                  <div class="col-lg-8">
                    <small class="text-muted">0  Transaksi Sukses Dari  0  Transaksi </small>
                  </div>
                  <div class="col-lg-4">
                    <button class="btn btn-sm btn-outline-secondary"><img src="<?= base_url() ?>assets/img/tokomainan/share.png" alt="">Bagikan</button>
                  </div>
                </div>
                <div class="row py-2">
                  <div class="col-lg-12">
                    <h4 style="color: orange;">Rp.<?= $mainan[0]->harga_mainan ?></h4>
                  </div>
                </div>
                <div class="row py-1">
                  <div class="col-lg-12">
                    <small class="text-muted">Stok : <strong><?= $mainan[0]->stok_mainan ?></strong></small>
                  </div>
                </div>
                <div class="row py-3">
                  <div class="col-lg-12">
                    <button class="btn btn-outline-secondary mr-lg-1"><i class="fa fa-heart"></i> Disukai</button>
                    <button class="btn btn-outline-warning mr-lg-1"type="submit">Beli Sekarang</button>
                  </div>
                </div>
                <div class="row py-5">
                  <div class="col-lg-12">
                    <small class="text-muted mr-2"><img src="<?= base_url() ?>assets/img/tokomainan/view.png" alt=""> Dilihat <strong>0</strong></small>
                    <small class="text-muted mr-2"><img src="<?= base_url() ?>assets/img/tokomainan/shipping.png" alt=""> Terkirim <strong>0</strong></small>
                    <small class="text-muted mr-2"><img src="<?= base_url() ?>assets/img/tokomainan/order.png" alt=""> Kondisi <strong>0</strong></small>
                    <small class="text-muted mr-2"><img src="<?= base_url() ?>assets/img/tokomainan/price-tag.png" alt=""> Min.Beli <strong>0</strong></small>
                    <small class="text-muted"><img src="<?= base_url() ?>assets/img/tokomainan/secure-shield.png" alt=""> Asuransi <strong>YA</strong></small>
                  </div>
                </div>
              </div>
          </div>
          <input type="hidden" name="id" value="<?php echo $mainan[0]->id_mainan ?>" />
          <input type="hidden" name="nama" value="<?php echo $mainan[0]->nama_mainan ?>" />
          <input type="hidden" name="harga" value="<?php echo $mainan[0]->harga_mainan ?>" />
          <input type="hidden" name="gambar" value="<?php echo $mainan[0]->img_mainan ?>" />
          <input type="hidden" name="qty" value="1" />
          <!-- /.row -->
          </form>
        </div>
        <!-- /.col-lg-12 -->
        <div class="col-lg-12 py-2">
          <p>
            <button class="btn btn-outline-secondary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><img src="<?= base_url() ?>assets/img/tokomainan/clipboards.png" alt="">
              Deskripsi
            </button>
          </p>
          <hr>
          <div class="collapse" id="collapseExample">
            <div class="card card-body">
              <p><?= $mainan[0]->deskripsi_mainan ?></p>
            </div>
          </div>
        </div>  
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-4 bg-dark">
      <div class="container text-center">
        <img src="<?= base_url() ?>/assets/img/tokomainan/iconTm.png" alt="">
        <p class="m-0  text-white">Copyright &copy; tokoMainan.com 2018</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="<?= site_url('Login/logout') ?>" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MESSAGE BOX-->

    <!-- START PRELOADS -->
    <audio id="audio-alert" src="<?= base_url() ?>assets/audio/alert.mp3" preload="auto"></audio>
    <audio id="audio-fail" src="<?= base_url() ?>assets/audio/fail.mp3" preload="auto"></audio>
    <!-- END PRELOADS -->  

    <!-- Bootstrap core JavaScript -->
    <script src="<?= base_url() ?>assets/css/bootstrap/jquery/jquery.js"></script>
    <script src="<?= base_url() ?>assets/css/bootstrap/js/bootstrap.bundle.min.js"></script>
    
    <!-- PLUGIN -->
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/bootstrap/bootstrap.min.js"></script>        
    <!-- END PLUGINS -->                

    <!-- THIS PAGE PLUGINS -->
    <script type='text/javascript' src='<?= base_url() ?>assets/js/plugins/icheck/icheck.min.js'></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
    
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/datatables/jquery.dataTables.min.js"></script>    

    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/bootstrap/bootstrap-datepicker.js"></script>                
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/bootstrap/bootstrap-file-input.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/bootstrap/bootstrap-select.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/tagsinput/jquery.tagsinput.min.js"></script>
    <!-- END PAGE PLUGINS -->

    <!-- START TEMPLATE -->
    
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins.js"></script>        
    <script type="text/javascript" src="<?= base_url() ?>assets/js/actions.js"></script>        
    <!-- END TEMPLATE -->
    <!-- END SCRIPTS --> 

  </body>

</html>
