<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Jual Beli Online Mainan</title>

    <!-- Bootstrap core CSS -->
    <link href="<?= base_url() ?>assets/css/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?= base_url() ?>assets/css/shop-homepage.css" rel="stylesheet">

  </head>

  <body>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="<?= site_url('tokomainan') ?>"><img src="<?= base_url() ?>assets/img/tokoMainan/iconTm.png" alt="" width="80%"></a>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a href="" class="mb-control" data-box="#mb-signout" >
                <i><img src="<?= base_url()  ?>assets/img/tokoMainan/out-sign.png" alt="" width="40%"></i>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <div class="container py-2">
      <div class="row">
        <div class="col-lg-12">
          <h2 class="text-center">Daftar Belanja</h2>
          <form action="<?php echo site_url('tokomainan/ubah_cart')?>" method="post" name="frmShopping" id="frmShopping" class="form-horizontal" enctype="multipart/form-data">
          <?php
            if ($cart = $this->cart->contents())
              {
           ?>

          <table class="table">
            <tr id= "main_heading">
              <td width="2%">No</td>
              <td width="10%">Gambar</td>
              <td width="33%">Item</td>
              <td width="17%">Harga</td>
              <td width="8%">Qty</td>
              <td width="20%">Jumlah</td>
              <td width="10%">Hapus</td>
            </tr>
          <?php
          // Create form and send all values in "shopping/update_cart" function.
          $grand_total = 0;
          $i = 1;

          foreach ($cart as $item):
          $grand_total = $grand_total + $item['subtotal'];
          ?>
            <input type="hidden" name="cart[<?php echo $item['id'];?>][id]" value="<?php echo $item['id'];?>" />
            <input type="hidden" name="cart[<?php echo $item['id'];?>][rowid]" value="<?php echo $item['rowid'];?>" />
            <input type="hidden" name="cart[<?php echo $item['id'];?>][name]" value="<?php echo $item['name'];?>" />
            <input type="hidden" name="cart[<?php echo $item['id'];?>][price]" value="<?php echo $item['price'];?>" />
            <input type="hidden" name="cart[<?php echo $item['id'];?>][gambar]" value="<?php echo $item['gambar'];?>" />
            <input type="hidden" name="cart[<?php echo $item['id'];?>][qty]" value="<?php echo $item['qty'];?>" />
            <tr>
              <td><?php echo $i++; ?></td>
              <td><img class="img-responsive" src="<?php echo base_url() . 'assets/img/upload/product/'.$item['gambar']; ?>" width="80%"/></td>
              <td><?php echo $item['name']; ?></td>
              <td><?php echo number_format($item['price'], 0,",","."); ?></td>
              <td><input type="text" class="form-control input-sm" name="cart[<?php echo $item['id'];?>][qty]" value="<?php echo $item['qty'];?>" /></td>
              <td><?php echo number_format($item['subtotal'], 0,",",".") ?></td>
              <td>
                <a href="<?php echo site_url('tokoMainan/hapus')?>/<?php echo $item['rowid'];?>">
                  <img src="<?= base_url() ?>assets/img/tokoMainan/x-button.png" alt="">
                </a>
              </td>
            <?php endforeach; ?>
            </tr>
            <tr>
              <td colspan="3"><b>Total Belanja: Rp <?php echo number_format($grand_total, 0,",","."); ?></b></td>
              <td colspan="4" align="right">
                <button type="submit" data-box="#myModal" class="btn btn-lg btn-outline-light mb-control">
                  <img src="<?= base_url() ?>assets/img/tokoMainan/cart_delete.png" alt="">
                </button>
                <button class='btn btn-lg btn-outline-light'  type="submit"><img src="<?= base_url() ?>assets/img/tokoMainan/refresh.png" alt=""></button>
                <a href="<?= site_url('tokomainan/check_out') ?>" class="btn btn-lg btn-outline-light">
                    <img src="<?= base_url() ?>assets/img/tokoMainan/checkout.png" alt="">
                </a>
              </td>
            </tr>

          </table>
          <?php
              }
            else
              {
                echo "<div class='alert alert-danger alert-dismissible fade show' role='alert'>
                          <strong>Uups!</strong> Keranjang Belanja masih kosong.
                          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                            <span aria-hidden='true'>&times;</span>
                          </button>
                        </div>"; 
              } 
          ?>
          </form>


            <!-- Message Box cart -->
              <div class="message-box animated fadeIn" data-sound="alert" id="myModal">
                  <div class="mb-container">
                      <div class="mb-middle">
                          <div class="mb-title">Kosongkan <strong>Keranjang</strong> ?</div>
                          <div class="mb-content">
                              <p>tekan tidak jika tidak setuju. tekan ya untuk setuju.</p>
                          </div>
                          <div class="mb-footer">
                              <div class="pull-right">
                                  <a href="<?= site_url('tokoMainan/hapus/all') ?>" class="btn btn-success btn-lg">Ya</a>
                                  <button class="btn btn-default btn-lg mb-control-close">Tidak</button>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
            <!--End message box-->
          
        </div>
        <!-- /.col-lg-12 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container -->
    
    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="<?= site_url('Login/logout') ?>" class="btn btn-success btn-lg">Yes</a>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MESSAGE BOX-->

    <!-- START PRELOADS -->
    <audio id="audio-alert" src="<?= base_url() ?>assets/audio/alert.mp3" preload="auto"></audio>
    <audio id="audio-fail" src="<?= base_url() ?>assets/audio/fail.mp3" preload="auto"></audio>
    <!-- END PRELOADS -->  

    <!-- Bootstrap core JavaScript -->
    <script src="<?= base_url() ?>assets/css/bootstrap/jquery/jquery.js"></script>
    <script src="<?= base_url() ?>assets/css/bootstrap/js/bootstrap.bundle.min.js"></script>
    
    <!-- PLUGIN -->
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/jquery/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/bootstrap/bootstrap.min.js"></script>        
    <!-- END PLUGINS -->                

    <!-- THIS PAGE PLUGINS -->
    <script type='text/javascript' src='<?= base_url() ?>assets/js/plugins/icheck/icheck.min.js'></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
    
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/datatables/jquery.dataTables.min.js"></script>    

    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/bootstrap/bootstrap-datepicker.js"></script>                
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/bootstrap/bootstrap-file-input.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/bootstrap/bootstrap-select.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins/tagsinput/jquery.tagsinput.min.js"></script>
    <!-- END PAGE PLUGINS -->

    <!-- START TEMPLATE -->
    
    <script type="text/javascript" src="<?= base_url() ?>assets/js/plugins.js"></script>        
    <script type="text/javascript" src="<?= base_url() ?>assets/js/actions.js"></script>        
    <!-- END TEMPLATE -->
    <!-- END SCRIPTS --> 

  </body>

</html>