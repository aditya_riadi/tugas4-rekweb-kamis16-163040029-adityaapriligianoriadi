<!DOCTYPE html>
<html lang="en" class="body-full-height">
  <head>
  	<title>Register TokoMainan</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- CSS INCLUDE -->        
    <link rel="stylesheet" type="text/css" id="theme" href="<?=base_url() ?>assets/css/theme-default.css"/>
    <!-- EOF CSS INCLUDE -->

  </head>
  <body>
  	<!-- START CONTAINER -->
    <div class="login-container">
    	<!-- START LOGIN-BOX -->
    	<div class="login-box animated fadeInDown">
    		<div class="login-logo"></div>
    		<!-- START LOGIN-BODY -->
			<div class="login-body">
				<?php if (validation_errors()) : ?>
  				<div class="alert alert-danger alert-dismissible" role="alert">
  					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  					<strong><?php echo validation_errors(); ?></strong>
          </div>
        <?php endif; ?>
        <?php echo form_open('login/register', 'class = "form-horizontal"');?>
          <div class="login-title"><strong>Register Your Account</strong></div>

        <?php if ($this->session->flashdata('Success')) :?>
          <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <?= $this->session->flashdata('Success'); ?>
          </div>
        <?php endif; ?>
        <form action="<?php echo site_url('login/register') ?>" method="post">

          <div class="form-group">
            <div class="col-md-12">
            <?php
              echo form_label('', 'fullname');
              echo form_input('fullname', '', 'class="form-control" id="fullname" placeholder="Fullname"');
            ?>
            </div>
          </div>

          <div class="form-group">
            <div class="col-md-12">
              <?php
                echo form_label('', 'username');
                echo form_input('username', '', 'class="form-control" id="username" placeholder="username"');
              ?>
            </div>
          </div>

          <div class="form-group">
            <div class="col-md-12">
              <?php
                echo form_label('', 'password');
                echo form_password('password', '', 'class="form-control" id="password" placeholder="Password"');
                ?>
              </div>
          </div>

          <div class="form-group">
            <div class="col-md-6">
                <?php echo form_submit('daftar', 'Register', 'class = "btn btn-info btn-block"'); ?>
              </div>
              <div class="col-md-6">
                <!-- <button type="submit" class="btn btn-primary">Login</button> -->
                <a href="<?php echo site_url('login') ?>" class="login-subtitle">Sign in</a>
              </div>
          </div>
        </form> 
        <?php echo form_close() ?>
                   
      </div>
        <!-- START LOGIN-FOOTER -->
        <div class="login-footer">
          <div class="pull-left">
              &copy; 2018 TokoMainan
          </div>
          <div class="pull-right">
              <a href="#">About</a> |
              <a href="#">Privacy</a> |
              <a href="#">Contact Us</a>
          </div>
        </div> 
        <!-- END LOGIN-FOOTER -->
    	</div>
    	<!-- END LOGIN-BOX -->
    </div>
    <!-- END CONTAINER -->
    <script type="text/javascript" src="<?= base_url() ?>assets/js/actions.js"></script> 
  </body>
</html>